format PE console

entry Start

include 'win32a.inc'

section '.data' data readable writable

	AskCell1 db 'Write  number (%d %d):',0
	MatrixD db 'Type matrix dimansion (no more than 4x4):', 0
	spaceStr db ', ', 0

	arrayAStr db 'Array A: ', 0
	enterLine db 10,13,0
	formatLine db '%d',10,13,0
	formatLin db '%d',0
	StrSep db ', ',0

	counter db ?
	n db ?
	nn db ?
	det dd 1 ; result det
	temp dd ? ; for input
	A dd 16 dup(?)
	B dd 9 dup (?)
	C dd 4 dup (?)
	val1 dd ?
	val2 dd ?
	val3 dd ?
	val4 dd ?
	val5 dd ?

section '.code' code readable executable

	Start:
		invoke printf, MatrixD

		; read matrix size
		push n
		push formatLin
		call [scanf]

		cmp [n], 4
		jg exit  ;if gather than exit (wrong val)
		cmp [n],0; if (n <= 0) exit
		jle exit

		mov al,[n]
		mul [n]
		mov [nn],al

		mov ebx, A
		mov [counter],0

	ArrayInput:
		mov eax,0
		mov al,[counter]
		div [n]
		mov ecx,0
		mov cl,ah
		mov ah,0
		inc eax ;get x and y coords in matrix
		inc ecx

		invoke printf, AskCell1,eax,ecx

		invoke scanf, formatLin, temp

		mov eax, [temp]
		mov [ebx], eax

		mov al, [counter]
		inc al
		cmp al, [nn] ; max arr size (nn)
		    jge FindDet
		add ebx,4 ; next element
		inc [counter]
		jmp ArrayInput

	 FindDet:
		mov al, 1
		cmp al, [nn]
		je FindDet1
		mov al, 4
		cmp al, [nn]
		je FindDet2
		mov al, 9
		cmp al, [nn]
		je FindDet3
		mov al, 16
		cmp al, [nn]
		je FindDet4

	FindDet1:
		mov eax,A
		mov eax,[eax]
		mov [det], eax
		jmp exit

	FindDet2:
		mov ebx,4
		mov eax,A
		mov edx,0
		c1:
		mov ecx,[eax]
		mov [C+edx],ecx
		dec ebx
		add eax,4
		add edx,4
		    cmp ebx,0
			jne c1


		stdcall FindDet2Func
		jmp exit
	FindDet3:
		mov ebx,9
		mov eax,A
		mov edx,0
		c2:
		mov ecx,[eax]
		mov [B+edx],ecx
		dec ebx
		add eax,4
		add edx,4
		    cmp ebx,0
			jne c2

		stdcall FindDet3Func
		jmp exit
	FindDet4:
		stdcall FindDet4Func
		jmp exit
	exit:

		invoke printf, formatLine, [det]
		call [getch]

		push 0
		call [ExitProcess]


;--------------------------------------------------------------------------
;Find det of 4x4 matrix...
FindDet4Func:
		; for 1 minor
		mov eax,[A+20]
		mov [B],eax
		mov eax,[A+24]
		mov [B+4],eax
		mov eax,[A+28]
		mov [B+8],eax
		mov eax,[A+36]
		mov [B+12],eax
		mov eax,[A+40]
		mov [B+16],eax
		mov eax,[A+44]
		mov [B+20],eax
		mov eax,[A+52]
		mov [B+24],eax
		mov eax,[A+56]
		mov [B+28],eax
		mov eax,[A+60]
		mov [B+32],eax

		stdcall FindDet3Func
		;invoke printf, formatLine, [det]

		mov eax,[det]
		mov ecx,[A]
		mul ecx
		mov [val3],eax

		; for 2 minor
		mov eax,[A+16]
		mov [B],eax
		mov eax,[A+24]
		mov [B+4],eax
		mov eax,[A+28]
		mov [B+8],eax
		mov eax,[A+32]
		mov [B+12],eax
		mov eax,[A+40]
		mov [B+16],eax
		mov eax,[A+44]
		mov [B+20],eax
		mov eax,[A+48]
		mov [B+24],eax
		mov eax,[A+56]
		mov [B+28],eax
		mov eax,[A+60]
		mov [B+32],eax

		stdcall FindDet3Func
	       ; invoke printf, formatLine, [det]

		mov eax,[det]
		mov ecx,[A+4]
		mul ecx
		mov [val4],eax

		; for 3 minor
		mov eax,[A+16]
		mov [B],eax
		mov eax,[A+20]
		mov [B+4],eax
		mov eax,[A+28]
		mov [B+8],eax
		mov eax,[A+32]
		mov [B+12],eax
		mov eax,[A+36]
		mov [B+16],eax
		mov eax,[A+44]
		mov [B+20],eax
		mov eax,[A+48]
		mov [B+24],eax
		mov eax,[A+52]
		mov [B+28],eax
		mov eax,[A+60]
		mov [B+32],eax

		stdcall FindDet3Func
		;invoke printf, formatLine, [det]

		mov eax,[det]
		mov ecx,[A+8]
		mul ecx
		mov [val5],eax

		; for 4 minor
		mov eax,[A+16]
		mov [B],eax
		mov eax,[A+20]
		mov [B+4],eax
		mov eax,[A+24]
		mov [B+8],eax
		mov eax,[A+32]
		mov [B+12],eax
		mov eax,[A+36]
		mov [B+16],eax
		mov eax,[A+40]
		mov [B+20],eax
		mov eax,[A+48]
		mov [B+24],eax
		mov eax,[A+52]
		mov [B+28],eax
		mov eax,[A+56]
		mov [B+32],eax

		stdcall FindDet3Func
		;invoke printf, formatLine, [det]

		mov ebx, [val3] ;minor1*a1
		sub ebx, [val4] ;minor1*a1 - minor2*a2
		add ebx, [val5] ;minor1*a1 - minor2*a2 + minor3*a3

		mov eax,[det]
		mov ecx,[A+12]
		mul ecx       ;minor4*a4 => eax
		sub ebx,eax   ;minor1*a1 - minor2*a2 + minor3*a3 - minor4*a4


		mov [det], ebx ;ret det
		ret
;--------------------------------------------------------------------------
;Find det of 3x3 matrix...
FindDet3Func:
		; for 1 minor
		mov eax,[B+16]
		mov [C],eax
		mov eax,[B+20]
		mov [C+4],eax
		mov eax,[B+28]
		mov [C+8],eax
		mov eax,[B+32]
		mov [C+12],eax

		stdcall FindDet2Func

		mov eax,[det]
		mov ecx,[B]
		mul ecx
		mov [val1],eax

		; for 2 minor
		mov eax,[B+12]
		mov [C],eax
		mov eax,[B+20]
		mov [C+4],eax
		mov eax,[B+24]
		mov [C+8],eax
		mov eax,[B+32]
		mov [C+12],eax

		stdcall FindDet2Func

		mov eax,[det]
		mov ecx,[B+4]
		mul ecx
		mov [val2],eax

		; for 3 minor
		mov eax,[B+12]
		mov [C],eax
		mov eax,[B+16]
		mov [C+4],eax
		mov eax,[B+24]
		mov [C+8],eax
		mov eax,[B+28]
		mov [C+12],eax

		stdcall FindDet2Func

		;calc
		mov eax,[val1]
		mov ebx,[val2]
		sub eax,ebx; eax-ebx <=> val1-val2 => eax
		mov [val1],eax

		mov eax,[det];minor3
		mov ecx,[B+8]
		mul ecx ;eax = minor3*a3

		mov ebx,[val1]
		add ebx,eax; nimor1*a1-minor2*a2+minor3*a3
		mov eax,ebx

		mov [det], eax ;ret det
		ret
;--------------------------------------------------------------------------
;Find det of 2x2 matrix...
FindDet2Func:
		xor eax,eax
		mov eax, [C]; esp = FinDet esp+4 = 10
		mov ecx, [C+12]
		mul ecx

		mov ebx,eax

		xor eax,eax
		mov eax, [C+4]; esp = FinDet esp+4 = 10
		mov ecx, [C+8]
		mul ecx

		sub eax, ebx
		neg eax

		mov [det], eax
		ret

section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess'

	import msvcrt,\
	       scanf, 'scanf',\
	       printf, 'printf',\
	       getch, '_getch'











