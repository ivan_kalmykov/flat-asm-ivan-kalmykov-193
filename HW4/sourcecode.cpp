#include <iostream>
#include <fstream>
#include <mutex>
#include <omp.h>

const std::string filenames[] = {"test1.txt","test2.txt","test3.txt","test4.txt"};

std::fstream* threads_pool;

int main(int argc, char** argv)
{
	try {
		std::string path_to_file = argv[2];

		threads_pool = new std::fstream[4];
		int numberOfThreads = atoi(((std::string)argv[1]).c_str());
		for (int i = 0; i < 4; i++)
		{
			std::string temporal = path_to_file;
			temporal.append(filenames[i]);
			threads_pool[i] = std::fstream(temporal, std::ios::in);

			if (!threads_pool[i].is_open())
				throw std::runtime_error("Error! Can't open file");
		}
			
		#pragma omp parallel num_threads(numberOfThreads)
		{
			do
			{
				char strbuf[100];
				std::string res = "(";
				int* numberr = new int[4];
				bool isLast = true;
				#pragma omp critical (nm)
				{
					for (int i = 0; i < 4; i++)
					{
						if (threads_pool[i].eof())
							isLast = false;
						threads_pool[i] >> numberr[i];
					}
				}
				for (int k = 0; k < 4; k++)
				{
					_itoa(numberr[k], strbuf, 10);
					res.append(strbuf);

					if (k < 3)
						res.append(", ");
				}
				res.append(")");
				#pragma omp critical (cout)
				{
					if (isLast)
						std::cout << res << std::endl;//���� ������ ������ ����� � ����...
						//cout << omp_get_thread_num() << ": " << res << endl;//for debug
				}
			} while (!threads_pool[0].eof());
		}
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;//������ endl �� \n\r or \n
	}
	return 0;
}